package com.founder.core.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "t_fiance_wx")
public class FianceWx implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "mchid")
    private String mchid;

    @Column(name = "date")
    private String date;

    @Column(name = "wxorder")
    private String wxorder;

    @Column(name = "bzorder")
    private String bzorder;

    @Column(name = "tradestatus")
    private String tradestatus;

    @Column(name = "time")
    private String time;

    @Column(name = "ghid")
    private String ghid;

    @Column(name = "merno")
    private String merno;

    @Column(name = "submch")
    private String submch;

    @Column(name = "deviceid")
    private String deviceid;

    @Column(name = "openid")
    private String openid;

    @Column(name = "tradetype")
    private String tradetype;

    @Column(name = "bank")
    private String bank;

    @Column(name = "currency")
    private String currency;

    @Column(name = "totalmoney")
    private BigDecimal totalmoney;

    @Column(name = "redpacketmoney")
    private BigDecimal redpacketmoney;

    @Column(name = "wxrefundorder")
    private String wxrefundorder;

    @Column(name = "bzrefundorder")
    private String bzrefundorder;

    @Column(name = "refundmoney")
    private BigDecimal refundmoney;

    @Column(name = "redpacketrefundmoney")
    private BigDecimal redpacketrefundmoney;

    @Column(name = "refundtype")
    private String refundtype;

    @Column(name = "refundstatus")
    private String refundstatus;

    @Column(name = "commodityname")
    private String commodityname;

    @Column(name = "datapacket")
    private String datapacket;

    @Column(name = "fee")
    private BigDecimal fee;

    @Column(name = "rate")
    private String rate;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWxorder() {
        return wxorder;
    }

    public void setWxorder(String wxorder) {
        this.wxorder = wxorder;
    }

    public String getBzorder() {
        return bzorder;
    }

    public void setBzorder(String bzorder) {
        this.bzorder = bzorder;
    }

    public String getTradestatus() {
        return tradestatus;
    }

    public void setTradestatus(String tradestatus) {
        this.tradestatus = tradestatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGhid() {
        return ghid;
    }

    public void setGhid(String ghid) {
        this.ghid = ghid;
    }

    public String getMerno() {
        return merno;
    }

    public void setMerno(String merno) {
        this.merno = merno;
    }

    public String getSubmch() {
        return submch;
    }

    public void setSubmch(String submch) {
        this.submch = submch;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getTradetype() {
        return tradetype;
    }

    public void setTradetype(String tradetype) {
        this.tradetype = tradetype;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getTotalmoney() {
        return totalmoney;
    }

    public void setTotalmoney(BigDecimal totalmoney) {
        this.totalmoney = totalmoney;
    }

    public BigDecimal getRedpacketmoney() {
        return redpacketmoney;
    }

    public void setRedpacketmoney(BigDecimal redpacketmoney) {
        this.redpacketmoney = redpacketmoney;
    }

    public String getWxrefundorder() {
        return wxrefundorder;
    }

    public void setWxrefundorder(String wxrefundorder) {
        this.wxrefundorder = wxrefundorder;
    }

    public String getBzrefundorder() {
        return bzrefundorder;
    }

    public void setBzrefundorder(String bzrefundorder) {
        this.bzrefundorder = bzrefundorder;
    }

    public BigDecimal getRefundmoney() {
        return refundmoney;
    }

    public void setRefundmoney(BigDecimal refundmoney) {
        this.refundmoney = refundmoney;
    }

    public BigDecimal getRedpacketrefundmoney() {
        return redpacketrefundmoney;
    }

    public void setRedpacketrefundmoney(BigDecimal redpacketrefundmoney) {
        this.redpacketrefundmoney = redpacketrefundmoney;
    }

    public String getRefundtype() {
        return refundtype;
    }

    public void setRefundtype(String refundtype) {
        this.refundtype = refundtype;
    }

    public String getRefundstatus() {
        return refundstatus;
    }

    public void setRefundstatus(String refundstatus) {
        this.refundstatus = refundstatus;
    }

    public String getCommodityname() {
        return commodityname;
    }

    public void setCommodityname(String commodityname) {
        this.commodityname = commodityname;
    }

    public String getDatapacket() {
        return datapacket;
    }

    public void setDatapacket(String datapacket) {
        this.datapacket = datapacket;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", mchid=").append(mchid);
        sb.append(", date=").append(date);
        sb.append(", wxorder=").append(wxorder);
        sb.append(", bzorder=").append(bzorder);
        sb.append(", tradestatus=").append(tradestatus);
        sb.append(", time=").append(time);
        sb.append(", ghid=").append(ghid);
        sb.append(", merno=").append(merno);
        sb.append(", submch=").append(submch);
        sb.append(", deviceid=").append(deviceid);
        sb.append(", openid=").append(openid);
        sb.append(", tradetype=").append(tradetype);
        sb.append(", bank=").append(bank);
        sb.append(", currency=").append(currency);
        sb.append(", totalmoney=").append(totalmoney);
        sb.append(", redpacketmoney=").append(redpacketmoney);
        sb.append(", wxrefundorder=").append(wxrefundorder);
        sb.append(", bzrefundorder=").append(bzrefundorder);
        sb.append(", refundmoney=").append(refundmoney);
        sb.append(", redpacketrefundmoney=").append(redpacketrefundmoney);
        sb.append(", refundtype=").append(refundtype);
        sb.append(", refundstatus=").append(refundstatus);
        sb.append(", commodityname=").append(commodityname);
        sb.append(", datapacket=").append(datapacket);
        sb.append(", fee=").append(fee);
        sb.append(", rate=").append(rate);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        FianceWx other = (FianceWx) that;
        return (this.getMchid() == null ? other.getMchid() == null : this.getMchid().equals(other.getMchid()))
            && (this.getDate() == null ? other.getDate() == null : this.getDate().equals(other.getDate()))
            && (this.getWxorder() == null ? other.getWxorder() == null : this.getWxorder().equals(other.getWxorder()))
            && (this.getBzorder() == null ? other.getBzorder() == null : this.getBzorder().equals(other.getBzorder()))
            && (this.getTradestatus() == null ? other.getTradestatus() == null : this.getTradestatus().equals(other.getTradestatus()))
            && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
            && (this.getGhid() == null ? other.getGhid() == null : this.getGhid().equals(other.getGhid()))
            && (this.getMerno() == null ? other.getMerno() == null : this.getMerno().equals(other.getMerno()))
            && (this.getSubmch() == null ? other.getSubmch() == null : this.getSubmch().equals(other.getSubmch()))
            && (this.getDeviceid() == null ? other.getDeviceid() == null : this.getDeviceid().equals(other.getDeviceid()))
            && (this.getOpenid() == null ? other.getOpenid() == null : this.getOpenid().equals(other.getOpenid()))
            && (this.getTradetype() == null ? other.getTradetype() == null : this.getTradetype().equals(other.getTradetype()))
            && (this.getBank() == null ? other.getBank() == null : this.getBank().equals(other.getBank()))
            && (this.getCurrency() == null ? other.getCurrency() == null : this.getCurrency().equals(other.getCurrency()))
            && (this.getTotalmoney() == null ? other.getTotalmoney() == null : this.getTotalmoney().equals(other.getTotalmoney()))
            && (this.getRedpacketmoney() == null ? other.getRedpacketmoney() == null : this.getRedpacketmoney().equals(other.getRedpacketmoney()))
            && (this.getWxrefundorder() == null ? other.getWxrefundorder() == null : this.getWxrefundorder().equals(other.getWxrefundorder()))
            && (this.getBzrefundorder() == null ? other.getBzrefundorder() == null : this.getBzrefundorder().equals(other.getBzrefundorder()))
            && (this.getRefundmoney() == null ? other.getRefundmoney() == null : this.getRefundmoney().equals(other.getRefundmoney()))
            && (this.getRedpacketrefundmoney() == null ? other.getRedpacketrefundmoney() == null : this.getRedpacketrefundmoney().equals(other.getRedpacketrefundmoney()))
            && (this.getRefundtype() == null ? other.getRefundtype() == null : this.getRefundtype().equals(other.getRefundtype()))
            && (this.getRefundstatus() == null ? other.getRefundstatus() == null : this.getRefundstatus().equals(other.getRefundstatus()))
            && (this.getCommodityname() == null ? other.getCommodityname() == null : this.getCommodityname().equals(other.getCommodityname()))
            && (this.getDatapacket() == null ? other.getDatapacket() == null : this.getDatapacket().equals(other.getDatapacket()))
            && (this.getFee() == null ? other.getFee() == null : this.getFee().equals(other.getFee()))
            && (this.getRate() == null ? other.getRate() == null : this.getRate().equals(other.getRate()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getMchid() == null) ? 0 : getMchid().hashCode());
        result = prime * result + ((getDate() == null) ? 0 : getDate().hashCode());
        result = prime * result + ((getWxorder() == null) ? 0 : getWxorder().hashCode());
        result = prime * result + ((getBzorder() == null) ? 0 : getBzorder().hashCode());
        result = prime * result + ((getTradestatus() == null) ? 0 : getTradestatus().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getGhid() == null) ? 0 : getGhid().hashCode());
        result = prime * result + ((getMerno() == null) ? 0 : getMerno().hashCode());
        result = prime * result + ((getSubmch() == null) ? 0 : getSubmch().hashCode());
        result = prime * result + ((getDeviceid() == null) ? 0 : getDeviceid().hashCode());
        result = prime * result + ((getOpenid() == null) ? 0 : getOpenid().hashCode());
        result = prime * result + ((getTradetype() == null) ? 0 : getTradetype().hashCode());
        result = prime * result + ((getBank() == null) ? 0 : getBank().hashCode());
        result = prime * result + ((getCurrency() == null) ? 0 : getCurrency().hashCode());
        result = prime * result + ((getTotalmoney() == null) ? 0 : getTotalmoney().hashCode());
        result = prime * result + ((getRedpacketmoney() == null) ? 0 : getRedpacketmoney().hashCode());
        result = prime * result + ((getWxrefundorder() == null) ? 0 : getWxrefundorder().hashCode());
        result = prime * result + ((getBzrefundorder() == null) ? 0 : getBzrefundorder().hashCode());
        result = prime * result + ((getRefundmoney() == null) ? 0 : getRefundmoney().hashCode());
        result = prime * result + ((getRedpacketrefundmoney() == null) ? 0 : getRedpacketrefundmoney().hashCode());
        result = prime * result + ((getRefundtype() == null) ? 0 : getRefundtype().hashCode());
        result = prime * result + ((getRefundstatus() == null) ? 0 : getRefundstatus().hashCode());
        result = prime * result + ((getCommodityname() == null) ? 0 : getCommodityname().hashCode());
        result = prime * result + ((getDatapacket() == null) ? 0 : getDatapacket().hashCode());
        result = prime * result + ((getFee() == null) ? 0 : getFee().hashCode());
        result = prime * result + ((getRate() == null) ? 0 : getRate().hashCode());
        return result;
    }
}