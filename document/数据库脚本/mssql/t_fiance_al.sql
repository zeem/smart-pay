
CREATE TABLE [dbo].[t_fiance_al](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[alorder] [varchar](100) NOT NULL,
	[bzorder] [varchar](100) NOT NULL,
	[tradetype] [varchar](50) NOT NULL,
	[bztitle] [varchar](1000) NULL,
	[createtime] [varchar](19) NULL,
	[tradetime] [varchar](19) NULL,
	[much] [varchar](50) NULL,
	[opera] [varchar](30) NULL,
	[deviceid] [varchar](20) NULL,
	[tradeuser] [varchar](100) NULL,
	[totalmoney] [decimal](18, 2) NULL,
	[trademoney] [decimal](18, 2) NOT NULL,
	[redpacketmoney] [decimal](18, 2) NULL,
	[jifenmoney] [decimal](18, 2) NULL,
	[alipaytax] [decimal](18, 2) NULL,
	[tradetax] [decimal](18, 2) NULL,
	[cancletax] [decimal](18, 2) NULL,
	[taxname] [varchar](200) NULL,
	[redpacketmoneytax] [decimal](18, 2) NULL,
	[cardtrademoney] [decimal](18, 2) NULL,
	[tforder] [varchar](100) NULL,
	[servicemoney] [decimal](18, 2) NULL,
	[fenrun] [decimal](18, 2) NULL,
	[mark] [varchar](1000) NULL,
	[date] [varchar](8) NULL,
	[mchid] [varchar](100) NOT NULL,
 CONSTRAINT [PK__t_fiance__3213E83F03F0984C] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


