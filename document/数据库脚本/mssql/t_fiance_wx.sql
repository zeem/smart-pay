
CREATE TABLE [dbo].[t_fiance_wx](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[time] [varchar](19) NULL,
	[ghid] [varchar](100) NULL,
	[merno] [varchar](200) NULL,
	[mchid] [varchar](100) NULL,
	[submch] [varchar](100) NULL,
	[deviceid] [varchar](50) NULL,
	[wxorder] [varchar](100) NULL,
	[bzorder] [varchar](100) NULL,
	[openid] [varchar](100) NULL,
	[tradetype] [varchar](30) NULL,
	[tradestatus] [varchar](20) NULL,
	[bank] [varchar](100) NULL,
	[currency] [varchar](30) NULL,
	[totalmoney] [decimal](18, 2) NULL,
	[redpacketmoney] [decimal](18, 2) NULL,
	[wxrefundorder] [varchar](100) NULL,
	[bzrefundorder] [varchar](100) NULL,
	[refundmoney] [decimal](18, 2) NULL,
	[redpacketrefundmoney] [decimal](18, 2) NULL,
	[refundtype] [varchar](20) NULL,
	[refundstatus] [varchar](20) NULL,
	[commodityname] [varchar](500) NULL,
	[datapacket] [varchar](max) NULL,
	[fee] [decimal](18, 2) NULL,
	[rate] [varchar](20) NULL,
	[date] [varchar](20) NOT NULL,
 CONSTRAINT [PK__t_fiance__3213E83F00200768] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


