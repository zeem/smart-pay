package com.founder.controller;

import com.founder.core.log.MyLog;
import com.founder.service.FianceAliService;
import com.founder.service.FianceWxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class IndexController {

    private final MyLog _log = MyLog.getLog(IndexController.class);

    @Autowired
    FianceWxService fianceWxService;

    @Autowired
    FianceAliService fianceAliService;

    @RequestMapping("/")
    @ResponseBody
    String index() {
        return "xxl job executor running.";
    }

    @RequestMapping(value = "/bill/zfb")
    @ResponseBody
    public String downbillAli(String service, HttpServletRequest request) {

        _log.info("###### 开始下载支付宝对账单 ######");
        String date = request.getParameter("date");//对账日期
        String mchId = request.getParameter("mchId");
        String prefix = request.getParameter("prefix");
        String suffix = request.getParameter("suffix");
        String ftpOpen = request.getParameter("ftpOpen");
        String ftpHome = request.getParameter("ftpHome");
        String subsys = request.getParameter("subsys");

        String result = fianceAliService.downloadBill(mchId, date, "ALIPAY_QR");

        fianceAliService.generateAl(mchId, date, prefix, suffix, ftpOpen, ftpHome, subsys);

        return result;
    }

    @RequestMapping(value = "/bill/wx")
    @ResponseBody
    public String downbillWx(String service, HttpServletRequest request) {

        _log.info("###### 开始下载微信对账单 ######");
        String date = request.getParameter("date");//对账日期
        String mchId = request.getParameter("mchId");
        String prefix = request.getParameter("prefix");
        String suffix = request.getParameter("suffix");
        String ftpOpen = request.getParameter("ftpOpen");
        String ftpHome = request.getParameter("ftpHome");
        String subsys = request.getParameter("subsys");

        String result = fianceWxService.downloadBill(mchId, date, "WX_NATIVE");

        fianceWxService.generateWx(mchId, date, prefix, suffix, ftpOpen, ftpHome, subsys);

        return result;
    }
}
