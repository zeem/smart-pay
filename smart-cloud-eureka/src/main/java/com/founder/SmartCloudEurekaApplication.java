package com.founder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SmartCloudEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartCloudEurekaApplication.class, args);
    }
}
